def index():
    if not request.args(0):
        redirect(URL('default', 'index'))

    t = Tournament(request.args(0))

    return dict(t=t)

def admin():

    db.Tournament.current_game.readable = db.Tournament.current_game.writable = False
    db.Tournament.next_game.readable = db.Tournament.next_game.writable = False

    crud.settings.formstyle = 'bootstrap3_stacked'
    update = crud.update(db.Tournament, request.args(0))

    return dict(update=update)

@auth.requires_login()
def create():
    # present form to create a new tournament

    # auto generate a 4-char join code
    code = id_generator(4)
    print code

    # check to make sure it's not in use
    code_check = db(db.Tournament.code == code).select()
    if code_check:
        # if it does exist, refresh page
        # probably a better way to do this with a while loop
        # TODO: make this more elegant
        redirect(URL('tournament', 'create'))
    else:
        db.Tournament.code.default = code
        db.Tournament.code.readable = db.Tournament.code.writable = False
        db.Tournament.started.readable = db.Tournament.started.writable = False
        db.Tournament.finished.readable = db.Tournament.finished.writable = False
        db.Tournament.current_game.readable = db.Tournament.current_game.writable = False
        db.Tournament.next_game.readable = db.Tournament.next_game.writable = False

    db.Tournament.name.label = "Tournament Name"
    # db.Tournament.name.comment = "%s %s's New Tournament" % (auth.user.first_name, auth.user.last_name)
    db.Tournament.name.default = "%s %s's New Tournament" % (auth.user.first_name, auth.user.last_name)
    db.Tournament.password.comment = "Optional. If entered, new players must provide password to join your tournament"
    db.Tournament.private.comment = "Hide tournament details from the public"

    form = SQLFORM(db.Tournament)

    if form.process().accepted:
        tourney = Tournament(form.vars.id)
        tourney.add_manager(auth.user_id)
        tourney.save()
        redirect(URL('tournament', 'view', args=(form.vars.id)))

    return dict(form=form)

@auth.requires_login()
def join():
    # expect either an id or a code
    # if tourney has pw, require pw

    code = None
    dbid = None

    if request.vars.code:
        # get tournament dbid of this code
        code = request.vars.code
        t = db(db.Tournament.code == code).select().first()
        if t:
            dbid = t.id
        else:
            response.flash = "Invalid Code"
            redirect(URL('tournament', 'list_all'))
    elif request.vars.id:
        dbid = request.vars.id
    else:
        redirect(URL('tournament', 'list_all'))

    tourney = Tournament(dbid)

    fields = []
    # if dbid:
    fields.append(Field('id', 'integer', default=dbid))
    # if code:
    fields.append(Field('code', 'string', default=code))
    # if tourney.password:
    fields.append(Field('password', 'password'))

    form = SQLFORM.factory(*fields)
    if form.process().accepted:
        if form.vars.password:
            if tourney.password == form.vars.password:
                # TODO: persist authentication for this tournament
                redirect(URL('tournament', 'index', args=dbid))
            else:
                response.flash = "Invalid Password"
                redirect(URL('tournament', 'join', vars={'id': dbid}))
        else:
            redirect(URL('tournament', 'index', args=dbid))
            pass

    return dict(form=form)

def view():
    if not request.args(0):
        redirect(URL('default', 'index'))

    tournament_id = db(db.Tournament.id == request.args(0)).select().first()
    tournament = Tournament(tournament_id)

    return dict(tournament=tournament)

def teams():
    # display all teams in this tournament, with join links if team is not private
    if not request.args(0):
        redirect(URL('default', 'index'))

    teams = Tournament(request.args(0)).teams
    # for team in teams:
    #     print team.name

    return dict(teams=teams)

def list_all():
    # list nearby and upcoming tournaments
    # for now, show list of all tournaments that are not finished

    # list of tournaments
    # on click, go to this tournament view
    ts = db(db.Tournament).select()

    # also display a join by code
    form = SQLFORM.factory(
        Field('code', label='Join Code', length=4, comment='Have a code? Enter it here'),
        formstyle='bootstrap3_stacked')
    # input-lg col-md-2
    form.custom.widget.code['_class'] = 'form-control string input-lg text-center'
    form.custom.widget.code['_maxlength'] = '4'
    if form.process().accepted:
        redirect(URL('tournament', 'join', vars={'code': form.vars.code}))

    return dict(ts=ts, form=form)

def standings():
    # get standings for provided tournament id
    if request.args(0):
        tournament_id = request.args(0)
        query = (db.Tournament_Team.tournament == tournament_id)&(db.Tournament_Team.team == db.Team.id)
        fields = []
        fields.append(db.Team.name)
        fields.append(db.Team.wins)
        fields.append(db.Team.losses)
        # fields.append(db.Team.win_percent)
        fields.append(db.Team.point_diff)
        s = SQLFORM.grid(query, fields, deletable=False, editable=False, details=False,create=False, searchable=False, csv=False, maxtextlength=100, user_signature=False)
        s.element('.web2py_counter', replace=None)
    else:
        s = "Choose a Tournament to view"
    return dict(s=s)

def games():
    # list all game results, with link to control
    # TODO: sort by round

    t = Tournament(request.args(0))
    return dict(t=t)

def layout():
    # define the structure (layout) of the tournament
    form = crud.create(db.Layout)
    return dict(form=form)

def start():
    t = Tournament(request.args(0))
    t.start()
    t.save()
    redirect(URL('tournament', 'view', args=request.args(0)))
    return dict()
