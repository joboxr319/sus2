def create():
    # manually create a game from the list of available teams

    return dict()

# try differnt methods for controlling the game
# i.e. XMLRPC, REST, etc

# first trying the rest API

# require login here? or at call()?
def control():
    # provide interface for start, update score, finish game
    # instead of redirecting if no id provided, or not authorized, display disabled buttons
    # TODO: enforce authorization
    allowed = True

    if request.args(0):
        g = Game(request.args(0))

        # which button to display?
        # if not allowed, disable button via css class
        c = "btn btn-default"
        if not allowed:
            c += " disabled"

        if not g.started:
            # display start button
            b = A('Start Game!', _class=c, _href=URL('game', 'start', args=g.id), _role="button")
        elif not g.finished:
            # display finish button
            b = A('Finish Game!', _class=c, _href=URL('game', 'finish', args=g.id), _role="button")
        else:
            # display link to view results
            b = A('View Results!', _class=c, _href=URL('game', 'view', args=g.id), _role="button")

        t = g.getTournamentID()

    else:
        g = None
        t = None

    return dict(g=g, allowed=allowed, b=b, t=t)

def start():
    # start game and enable control
    g = Game(request.args(0))
    g.start()
    g.save()
    redirect(URL('game', 'control', args=g.id))
    return dict(g=g)

def finish():
    # finish game and disable control
    g = Game(request.args(0))
    g.finish()
    g.save()
    redirect(URL('game', 'control', args=g.id))
    return dict(g=g)

def home_score():
    # display the current score
    # if given a 2nd argument, update the game with that new score
    g = Game(request.args(0))
    n = request.args(1)
    s = int(n)+1 if n else g.home_score
    g.home_score = s
    g.save()
    return dict(g=g, s=s)

def away_score():
    # display the current score
    # if given a 2nd argument, update the game with that new score
    g = Game(request.args(0))
    n = request.args(1)
    s = int(n)+1 if n else g.away_score
    g.away_score = s
    g.save()

    return dict(g=g, s=s)

def reset():
    # reset game score
    # just the score or start/finish?
    g = Game(request.args(0))
    g.home_score = 0
    g.away_score = 0
    g.save()
    return dict(g=g)