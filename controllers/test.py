def register_user(player_name):
    # for testing purposes, create auth for fake players

    split_name = player_name.split(' ', 1)
    first_name = split_name[0]
    last_name = split_name[1]
    registration_id = "http://oauth.localhost:8000/%s" % id_generator(8)

    print first_name, last_name

    user_id = db.auth_user.insert(first_name=first_name, last_name=last_name, registration_id=registration_id)
    group_id = auth.add_group('user_%s' % user_id)
    auth.add_membership(group_id, user_id)

    return user_id

# test
def create_teams(teams=10):

    new_teams = []

    # create players
    # add newly registered users to the Player table
    users = db(db.auth_user).select()
    for user in users:
        user_id = user.id
        name = user.first_name
        if user.last_name:
            name = name + " " + user.last_name

        # check if a player with this user_id already exists. if not, add it
        user_is_player = db(db.Player.user_id == user_id).select().first()
        if not user_is_player:
            db.Player.insert(name=name, user_id=user_id)

    # list of all existing players:
    players = db(db.Player).select(db.Player.id).as_list()

    if not players or len(players) < (teams * 2):
        for i in range(0, (teams * 2)-len(players)):
            # create a new player to make list up to 20
            new_player = Player()
            new_player.name = fake.name()
            new_player.user_id = register_user(new_player.name)
            new_player.save()
            i += 1

    # refresh players
    players = db(db.Player).select(db.Player.id).as_list()

    for team in range(0, teams):

        new_team = Team()

        # first create team name
        list1 = ["Watery", "Frozen", "Burning", "Fiery", "Floating", "Dark", "Shadowed"]
        list2 = ["Tower", "Dungeon", "Stronghold", "Fort", "Cave", "Gauntlet", "Palace", "Castle"]
        list3 = ["Doom", "Dragons", "Despair", "Moans", "Cockles", "Traps", "Wizards", "Darkness"]

        sel1 = random.randint(0, len(list1)-1)
        sel2 = random.randint(0, len(list2)-1)
        sel3 = random.randint(0, len(list3)-1)

        team_name = list1[sel1] + " " + list2[sel2] + " " + list3[sel3]
        new_team.name = team_name

        # retrieve 2 existing players from the db
        for p in range(0,2):
            # get random player from players list (using list index, not db id)
            players_index = random.randint(0, len(players)-1)
            dbid = players[players_index]['id']
            new_player = Player(dbid)
            players.remove(players[players_index])

            # make sure player is fake registered
            if not new_player.user_id:
                new_player.user_id = register_user(new_player.name)
                new_player.save()

            new_team.add_member(new_player)

            # if this is the first player on the team, make them the manager
            if p == 1:
                new_team.add_manager(new_player)

        new_team.save()
        new_teams.append(new_team)

    return new_teams

def index():
    name = 'Hello'

    return dict(name=name)

def auto_play():
    # add 10 teams to the sign up sheet
    sus = create_teams(10)

    # Crete the tournament
    tourney = Tournament()
    tourney.code = id_generator(4)
    tourney.name = "Tourney %s" % tourney.code

    for team in sus:
        tourney.add_team(team)

    tourney.save()

    # run the games, winner stays, loser goes to bottom of list
    i = 1
    prev_game = None
    while i < 21: # play 20 games
        # which teams are playing?
        # for tourney start, use teams 1 and 2
        if not prev_game:
            home = sus[0]
            away = sus[1]
        else:
            # home will be winner of last match
            home = prev_game.winner
            # away will be next team on sus, as long as its not the winner, nor the previous loser
            away = sus[i]
            if away == home:
                away = sus[i+1]
                print "Can't play self"
            if away == prev_game.loser:
                away = sus[i+1]
                print "Can't play twice in a row"

        # create the game
        current_game = Game(None, home, away)

        # add it to the tournament
        game_number = tourney.add_game(current_game)
        print "\nGame %s: %s (%s-%s) vs. %s (%s-%s)" % (game_number, home.name, home.wins, home.losses, away.name, away.wins, away.losses)

        # play game
        current_game.update_score(home, random.randint(0,10))
        current_game.update_score(away, random.randint(0,10))

        # end game
        current_game.finish()
        if current_game.finished:
            print current_game.winner.name + " defeats " + current_game.loser.name + " " + str(current_game.winner.score) + " to " + str(current_game.loser.score)

        # add loser back to end of sus
        sus.append(current_game.loser)
        prev_game = current_game

        i += 1
    return tourney

def get_standings():

    # db.Tournament.truncate()
    # db.Game.truncate()
    # db.Team.truncate()
    # db.Player.truncate()
    #
    # tourney = auto_play()
    # standings = []
    #
    # for game in tourney.games:
    #     if game.home not in standings:
    #         standings.append(game.home)
    #     if game.away not in standings:
    #         standings.append(game.away)
    #
    # # sorted by wins
    # for team in sorted(standings, key=lambda team: team.wins, reverse=True):
    #     print "%s( %s-%s %s/%s %s %s)" % (team.name, team.wins, team.losses, team.points_for, team.points_against, team.win_percent, team.point_diff)

    query = (db.Tournament_Team.tournament == 1)&(db.Tournament_Team.team == db.Team.id)
    fields = []
    fields.append(db.Team.name)
    fields.append(db.Team.wins)
    fields.append(db.Team.losses)
    # fields.append(db.Team.win_percent)
    fields.append(db.Team.point_diff)
    standings = SQLFORM.grid(query, fields, deletable=False, editable=False, details=False,create=False, searchable=False, csv=False, maxtextlength=100)
    standings.element('.web2py_counter', replace=None)

    # print "\n"
#
    # # sorted by total points
    # for team in sorted(standings, key=lambda team: team.points_for, reverse=True):
    #     print "%s( %s-%s %s/%s %s %s)" % (team.name, team.wins, team.losses, team.points_for, team.points_against, team.win_percent, team.point_diff)
    #
    # # sorted by win percentage
    # for team in sorted(standings, key=lambda team: team.win_percent, reverse=True):
    #     print "%s( %s-%s %s/%s %s %s)" % (team.name, team.wins, team.losses, team.points_for, team.points_against, team.win_percent, team.point_diff)
    #     print "%s( %s-%s %s/%s %s %s)" % (team.name, team.wins, team.losses, team.points_for, team.points_against, team.win_percent, team.point_diff)

    # for key, value in vars(tourney).iteritems():
    #     for game in value:
    #         for key, value in vars(game).iteritems():
    #             print key, value

    return dict(standings=standings)\

def addFakeTeam():
    # add a fake team to an existing tournament
    # if 2nd argument is provided, add that many teams
    count = request.args(1) if request.args(1) else 1
    t = Tournament(request.args(0))
    teams = create_teams(count)
    for team in teams:
        t.add_team(team)
        t.save()

    redirect(URL('tournament', 'view', args=t.id))

    return dict()

def nextGame():
    # test to see if I can use a placeholder for "winner of previous game"
    # use Team object without saving it?
    # next team vs in progress home or away
    pass


    return dict()

def addColors():
    teams = db(db.Team).select()
    for t in teams:
        team = Team(t.id)
        if not t.primary_color:
            team.primary_color = random_color()
            team.save()
        if not t.secondary_color:
            team.secondary_color = random_color()
            team.save()
    return dict()
