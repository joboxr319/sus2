import string
import random
from faker import Faker
fake = Faker()

class Tournament(object):
    # if given a dbid, set class attributes from database value
    # TODO: if has password, require password
    def __init__(self, dbid=None, _password=None):
        _table = self.__class__.__name__
        self.fields = db[_table].fields
        for f in self.fields:
            setattr(self, f, None)

        self.games = []
        self.teams = []
        self.managers = []

        if dbid:
            row = db(db[_table].id == dbid).select().first().as_dict()
            self.dbid = dbid
            for key, value in row.iteritems():
                setattr(self, key, value)

            #  get any existing game id's and add them to self.games
            _games = db(db.Tournament_Game.tournament == self.dbid).select(db.Tournament_Game.game)
            if _games:
                for game in _games:
                    self.games.append(Game(game.game))

            #  get any existing team id's and add them to self.teams
            _teams = db(db.Tournament_Team.tournament == self.dbid).select(db.Tournament_Team.team)
            if _teams:
                for team in _teams:
                    self.teams.append(Team(team.team))

            #  get any existing manager id's and add them to self.managers
            _managers = db(db.Tournament_Manager.tournament == self.dbid).select(db.Tournament_Manager.manager)
            if _managers:
                for manager in _managers:
                    self.managers.append(manager.manager)

        else:
            # create db record and get name and id
            self.dbid = db[_table].insert()
            row = db(db[_table].id == self.dbid).select().first().as_dict()
            for key, value in row.iteritems():
                setattr(self, key, value)

    def save(self):
        _table = self.__class__.__name__
        for key, value in vars(self).iteritems():
            if key not in ['dbid', 'fields', 'id', 'games', 'teams', 'managers']:
                db(db[_table]._id == self.dbid).update(**{key:value})
                # print key, value

    def add_game(self, game):
        self.games.append(game)
        db.Tournament_Game.insert(tournament=self.dbid, game=game.dbid)
        game.overall_game_number = len(self.games)
        game.save()
        return len(self.games)

    def add_team(self, team):
        self.teams.append(team)
        db.Tournament_Team.insert(tournament=self.dbid, team=team.dbid)
        team.save()
        return len(self.teams)

    def remove_team(self, team):
        for t in self.teams:
            if t.id == team.id:
                self.teams.remove(t)
        db(db.Tournament_Team.team == team.id).delete()
        self.save()

    def add_manager(self, manager):
        self.managers.append(manager)
        db.Tournament_Manager.insert(tournament=self.dbid, manager=manager)
        return len(self.managers)

    # game flow management
    # TODO: move these to inherited classes which alter the rules based on Tournament layout
    # assuming type = sus for now
    def start(self, game=None):
        # start the tournament
        self.started = True

        # create the first game
        # if game object isn't provided, use the first two teams
        if not game:
            # make sure there are at least 2 teams
            if len(self.teams) >= 2:
                g = Game(None, self.teams[0], self.teams[1])
                g.save()
                self.add_game(g)
            else:
                return "Must have at least 2 teams"

    def in_progress(self):
        # get list of games that are currently in progress
        game_list = []
        for game in self.games:
            if game.started and not game.finished:
                game_list.append(game)
        return game_list

    def next_team(self, offset=0):
        # for sus, who is the next team that should play?
        # wont it always be away +1 (or + number of matches in progress?)
        # or ... when a game is started, it can remove the teams from the top of the list and put them at bottom?
        # list would need to persist in db
        # use auto id as position, or separate field?
        # assuming for now that it will be first team returned by tournament_team query

        tt = db(db.Tournament_Team.tournament == self.id).select(db.Tournament_Team.team)
        t = Team(tt[offset].team)

        return t

    def create_next_game(self, prev_game_winner=None):
        if prev_game_winner:
            home = prev_game_winner
            away = self.next_team()
        else:
            home = self.next_team()
            away = self.next_team(1)

        g = Game(None, home, away)
        g.save()
        self.add_game(g)
        self.save()
        print home.name, 'vs', away.name

class Game(object):

    # if given a dbid, set class attributes from database value
    def __init__(self, dbid=None, home=None, away=None):
        _table = self.__class__.__name__
        self.fields = db[_table].fields
        for f in self.fields:
            setattr(self, f, None)

        if dbid:
            row = db(db[_table].id == dbid).select().first().as_dict()
            self.dbid = dbid
            for key, value in row.iteritems():
                setattr(self, key, value)

            # get home and away teams from database
            teams = db(db.Game_Team.game == dbid).select().first()
            if teams:
                self.home = Team(teams.home)
                self.away = Team(teams.away)

        else:
            # create db record and get id
            self.dbid = db[_table].insert()
            row = db(db[_table].id == self.dbid).select().first().as_dict()
            for key, value in row.iteritems():
                setattr(self, key, value)
            self.home = home
            self.away = away
            db.Game_Team.insert(game=self.dbid, home=home.dbid, away=away.dbid)

        self.winner = None
        self.loser = None

    def save(self):
        _table = self.__class__.__name__
        for key, value in vars(self).iteritems():
            if key not in ['dbid', 'fields', 'id', 'home', 'away', 'winner', 'loser']:
                db(db[_table]._id == self.dbid).update(**{key:value})
                # print key, value

    def getTournamentID(self):
        tg = db(db.Tournament_Game.game == self.id).select(db.Tournament_Game.tournament).first()
        tid = tg.tournament
        return tid

    def start(self):
        if self.home and self.away:
            # move these 2 teams to the bottom of the list
            t = Tournament(self.getTournamentID())
            t.remove_team(self.home)
            t.remove_team(self.away)
            t.save()
            t.add_team(self.away)
            t.add_team(self.home)
            t.save()

            self.started = True
            self.save()

        else:
            return "Need 2 teams to play"

    def update_score(self, team, score):
        if team == self.home:
            self.home_score += score
        if team == self.away:
            self.away_score += score
        self.save()

    def getWinner(self):
        # if game is finished, return team id of winner
        if self.home_score >= self.away_score:
            winner = self.home.id
        else:
            winner = self.away.id
        return winner

    def finish(self):
        self.finished = True
        self.save()

        # finalize stats and assign to teams
        if self.home_score >= self.away_score:
            self.winner = self.home
            self.winner.score = self.home_score
            self.loser = self.away
            self.loser.score = self.away_score
        else:
            self.winner = self.away
            self.winner.score = self.away_score
            self.loser = self.home
            self.loser.score = self.home_score

        self.winner.wins += 1
        self.winner.points_for += self.winner.score
        self.winner.points_against += self.loser.score
        self.loser.losses += 1
        self.loser.points_for += self.loser.score
        self.loser.points_against += self.winner.score
        self.winner.stats()
        self.loser.stats()
        self.winner.save()
        self.loser.save()

        # trigger next game of tournament
        t = Tournament(self.getTournamentID())
        t.create_next_game(self.winner)
        t.save()

class Team(object):
    # if given a dbid, set class attributes from database value
    def __init__(self, dbid=None):
        _table = self.__class__.__name__

        self.members = []
        self.managers = []

        if dbid:
            row = db(db[_table].id == dbid).select().first().as_dict()
            self.dbid = dbid
            for key, value in row.iteritems():
                setattr(self, key, value)

            _members = db(db.Team_Player.team == dbid).select(db.Team_Player.player)
            if _members:
                for player in _members:
                    player_id = player.player
                    self.members.append(Player(player_id))

            _group = db(db.auth_group.role == 'team_%s' % dbid).select().first()
            if not _group:
                _managers = db(db.Team_Manager.team == dbid).select()
                for manager in _managers:
                    self.managers.append(manager.manager)
            else:
                _managers = db(db.auth_membership.group_id == _group.id).select()
                for manager in _managers:
                    self.managers.append(manager.user_id)
        else:
            # create db record and get id
            self.dbid = db[_table].insert()
            row = db(db[_table].id == self.dbid).select().first().as_dict()
            for key, value in row.iteritems():
                setattr(self, key, value)
            self.members = []

    def save(self):
        _table = self.__class__.__name__
        for key, value in vars(self).iteritems():
            if key not in ['dbid', 'fields', 'id', 'members', 'managers']:
                db(db[_table]._id == self.dbid).update(**{key:value})
                # print key, value

    def add_member(self, member):
        self.members.append(member)
        player_id = member.dbid
        # print player_id
        db.Team_Player.insert(team=self.dbid, player=player_id)

    def add_manager(self, player):
        self.managers.append(player.dbid)
        # first make sure this auth group exists
        # if not, create it
        group_name = 'team_%s' % self.dbid
        group = db(db.auth_group.role==group_name).select().first()
        if not group:
            group_id = db.auth_group.insert(role=group_name, description='manager')
        else:
            group_id = group.id

        # now add this user this team's auth group
        user_id = player.user_id
        auth.add_membership(group_id, user_id)

        # and even though it probably isn't necessary, add to Team_Manager too
        tm = db(db.Team_Manager.team == self.dbid).select().first()
        if not tm:
            db.Team_Manager.insert(team=self.dbid, manager=player.dbid)

    def stats(self):
        if self.wins > 0:
            if self.wins > 0:
                self.win_percent = self.wins / float(self.wins + self.losses)
            else:
                self.win_percent = 0
            self.point_diff = self.points_for - self.points_against
            self.save()

class Player(object):
    # if given a dbid, set class attributes from database value
    def __init__(self, dbid=None):
        _table = self.__class__.__name__
        self.fields = db[_table].fields
        for f in self.fields:
            setattr(self, f, None)
        if dbid:
            row = db(db[_table].id == dbid).select().first().as_dict()
            self.dbid = dbid
            for key, value in row.iteritems():
                setattr(self, key, value)
        else:
            # create db record and get name and id
            self.dbid = db[_table].insert()
            row = db(db[_table].id == self.dbid).select().first().as_dict()
            for key, value in row.iteritems():
                setattr(self, key, value)

    def save(self):
        _table = self.__class__.__name__
        for key, value in vars(self).iteritems():
            if key not in ['dbid', 'fields', 'id']:
                db(db[_table]._id == self.dbid).update(**{key:value})
                # print key, value


# functions

def id_generator(size=4, chars=string.ascii_uppercase + string.digits):
    # generate a random alphanumeric string
    return ''.join(random.choice(chars) for _ in range(size))

def random_color():
    color = "#%06x" % random.randint(0, 0xFFFFFF)
    return color

def register_player():
    # if not a current player, but logged in, add to players
    if not auth.user_id:
        redirect(URL('default', 'user'))
    else:
        # does a player with this user id exist?
        r = db(db.Player.user_id == auth.user_id).select(db.Player.id).first()
        if not r:
            # no? Then create a new player
            name = auth.user.first_name
            if auth.user.last_name:
                name = name + " " + auth.user.last_name
            player = Player()
            player.name = name
            player.user_id = auth.user_id
            player.save()
        else:
            # Yes? Then id the player object
            player = Player(r.id)
    return player


